import Ember from 'ember';

export default Ember.Service.extend({
    errors: [],
    notices: [],
    confirmations: [],
    addError(error) {
        this.get('errors').pushObject(error);
    },
    removeError(error) {
        this.get('errors').removeObject(error);
    },
    addNotice(notice) {
        this.get('notices').pushObject(notice);
    },
    removeNotice(notice) {
        this.get('notices').removeObject(notice);
    },
    addConfirmation(confirmation) {
        this.get('confirmations').pushObject(confirmation);
    },
    removeConfirmation(confirmation) {
        this.get('confirmations').removeObject(confirmation);
    },
    clear() {
        this.get('confirmations').clear();
        this.get('errors').clear();
        this.get('notices').clear();
    }
});
