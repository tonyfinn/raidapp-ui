import Ember from 'ember';

var Promise = Ember.RSVP.Promise;

export default Ember.Service.extend({
    modalText: '',
    modalTitle: '',
    modalShown: false,
    modalPromiseActions: null,
    showModal(title, text) {
        this.set('modalTitle', title);
        this.set('modalText', text);
        this.set('modalShown', true);
        return new Promise((resolve, reject) => {
            this.set('modalPromiseActions', { resolve, reject });
        });
    },
    cancelModal() {
        this.set('modalShown', false);
        this.get('modalPromiseActions').reject();
    },
    closeModal() {
        this.set('modalShown', false);
        this.get('modalPromiseActions').resolve();
    }
});
