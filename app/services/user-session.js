import Ember from 'ember';

export default Ember.Service.extend({
    currentSession: null,
    currentUser: null,
    store: Ember.inject.service(),

    init() {
        this.get('store').queryRecord('session', {}).then((session) => {
            this.set('currentSession', session);
            this.updateUser();
        }).catch(function () {
            console.log('User not logged in');
        });
    },

    updateUser() {
        var currentSession = this.get('currentSession');
        var store = this.get('store');
        var user = store.findRecord('user', currentSession.get('id'));
        if(!user) {
            user = store.createRecord('user', {
                id: currentSession.get('id'),
                name: currentSession.get('name'),
                email: currentSession.get('email')
            });
        }
        this.set('currentUser', user);
    },

    login(session) {
        return session.save().then(() => {
            this.set('currentSession', session);
            this.updateUser();
        });
    },

    logout() {
        return this.get('currentSession').destroyRecord().then(() => {
            this.set('currentSession', null);
            this.set('currentUser', null);
        });
    }
});
