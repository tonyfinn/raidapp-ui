import Ember from 'ember';

export default Ember.Component.extend({
    sessionService: Ember.inject.service('user-session'),
    modal: Ember.inject.service('modal'),
    alerts: Ember.inject.service('alerts'),
    store: Ember.inject.service(''),
    submit() {
        let user = this.get('user');
        if(user.get('name') && user.get('password') && user.get('email')) {
            user.save().then(() => {
                var newSession = this.get('store').createRecord('session');
                newSession.set('email', user.get('email'));
                newSession.set('password', user.get('password'));
                return this.get('sessionService').login(newSession);
            }).then(() => {
                this.sendAction();
            }).catch(err => {
                this.get('alerts').addError(err.errors[0].detail);
            });
        }
        return false;
    }
});
