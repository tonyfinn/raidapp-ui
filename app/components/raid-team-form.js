import Ember from 'ember';

export default Ember.Component.extend({
    sessionService: Ember.inject.service('user-session'),
    submit() {
        let team = this.get('team');
        team.set('owner', this.get('sessionService').get('currentUser'));
        if(team.get('name')) {
            team.save();
        }
        return false;
    }
});
