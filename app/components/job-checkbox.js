import Ember from 'ember';

export default Ember.Component.extend({
    classNames: ['player-job-info', 'row'],
    jobInfo: null,
    change() {
        console.log(this.get('jobInfo'));
        this.get('toggle')(this.get('jobInfo'));
    }
});
