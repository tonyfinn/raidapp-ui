import Ember from 'ember';

export default Ember.Component.extend({
    team: null,
    newMember: null,
    sessionService: Ember.inject.service('user-session'),
    modalService: Ember.inject.service('modal'),
    actions: {
        addMember(member) {
            if(!member) {
                this.get('modalService').showModal('No such member', 'You have not specified an existing character');
            } else {
                if(member.get('user').get('id') === this.get('sessionService').get('currentUser').get('id')) {
                    member.set('team', this.get('team'));
                    member.save();
                }
            }
        }
    }
});
