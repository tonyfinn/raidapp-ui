import Ember from 'ember';

export default Ember.Component.extend({
    modal: Ember.inject.service('modal'),
    classNames: ['modal-backdrop', 'fade'],
    classNameBindings: ['in', 'visible:show:invisible'],
    visible: Ember.computed.oneWay('modal.modalShown'),
    'in': Ember.computed.oneWay('modal.modalShown')
});
