import Ember from 'ember';

export default Ember.Component.extend({
    modal: Ember.inject.service('modal'),
    classNames: ['modal', 'fade'],
    classNameBindings: ['in', 'visible:show:invisible'],
    visible: Ember.computed.oneWay('modal.modalShown'),
    'in': Ember.computed.oneWay('modal.modalShown'),
    actions: {
        closeModal() {
            this.get('modal').closeModal();
        },
        cancelModal() {
            this.get('modal').cancelModal();
        }
    }
});
