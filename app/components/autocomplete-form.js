import Ember from 'ember';

export default Ember.Component.extend({
    itemComponent: null,
    autocompleteEntries: [],
    classNames: ['dropdown'],
    classNameBindings: ['open', 'labelLess:form-group'],
    modelType: null,
    store: Ember.inject.service(),
    displayField: 'name',
    selectedItem: null,
    searchTerm: '',
    open: false,
    init() {
        this._super();
        var selectedItem = this.get('selectedItem');
        if(selectedItem && selectedItem.then) {
            selectedItem.then((item) => {
                this.setInitialData(item);
            });
        } else {
            this.setInitialData(selectedItem);
        }
    },
    setInitialData(selectedItem) {
        if(!selectedItem) {
            return;
        }
        var displayField = this.get('displayField');
        var searchTerm = selectedItem.get(displayField);
        this.set('searchTerm', searchTerm);
    },
    actions: {
        updateEntries() {
            var store = this.get('store');
            var modelType = this.get('modelType');
            var searchTerm = this.get('searchTerm');
            var entries = store.query(modelType, { filter: { startswith: searchTerm }});
            entries.then((entryData) => {
                if(entryData) {
                    if(entryData.get('length') === 0) {
                        this.set('selectedItem', null);
                    } else if(entryData.get('length') === 1) {
                        this.set('selectedItem', entryData.objectAt(0));
                    }
                    this.set('open', true);
                } else {
                    this.set('open', false);
                }
                this.set('autocompleteEntries', entryData);
            });
        },
        selectEntry(entry) {
            var displayField = this.get('displayField');
            var displayName = entry.get(displayField);
            this.set('searchTerm', displayName);
            this.set('selectedItem', entry);
            this.set('open', false);
            return false;
        }
    }
});
