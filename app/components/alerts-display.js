import Ember from 'ember';

export default Ember.Component.extend({
    alerts: Ember.inject.service('alerts'),
    errors: Ember.computed.alias('alerts.errors'),
    notices: Ember.computed.alias('alerts.notices'),
    confirmations: Ember.computed.alias('alerts.confirmations'),
    actions: {
        removeError(err) {
            this.get('alerts').removeError(err);
        },
        removeNotice(notice) {
            this.get('alerts').removeNotice(notice);
        },
        removeConfirmation(confirm) {
            this.get('alerts').removeConfirmation(confirm);
        }
    }
});
