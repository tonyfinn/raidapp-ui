import Ember from 'ember';

export default Ember.Component.extend({
    store: Ember.inject.service('store'),
    groupedJobs: null,
    servers: null,
    server: null,
    playerJobs: null,
    init() {
        this._super();
        var groupedJobs = {};
        var store = this.get('store');
        var playerJobs = {};
        this.set('groupedJobs', groupedJobs);
        this.set('playerJobs', playerJobs);
        store.findAll('job').then((jobs) => {
            jobs.forEach((job) => {
                playerJobs[job.get('shortName')] = this.getJobInfo(job);
                job.get('roles').forEach((role) => {
                    var roleKey = (role === 'dps') ? 'DPS' : role.capitalize();
                    if(!groupedJobs[roleKey]) {
                        groupedJobs[roleKey] = [job];
                    } else {
                        groupedJobs[roleKey].push(job);
                    }
                });
            });
            Object.keys(groupedJobs).forEach(group => {
                groupedJobs[group] = groupedJobs[group].sort((a, b) => {
                    return b.get('name') < a.get('name');
                });
            });
            this.set('groupedJobs', groupedJobs);
            this.rerender();
        });
        store.findAll('server').then((servers) => {
            this.set('servers', servers);
        });
    },
    getJobInfo(job) {
        var characterJobs = this.get('character').get('characterJobs');
        var jobInfo = {
            selected: false,
            job: job,
            level: 60
        };
        characterJobs.then(cjobs => {
            jobInfo.selected  = cjobs.any(cjob => {
                return cjob.id === job.id;
            });
        });

        return jobInfo;
    },
    actions: {
        createCharacter() {
            let character = this.get('character');
            if(character.get('name')) {
                let allJobInfo = this.get('playerJobs');
                let selectedJobs = Object.keys(allJobInfo).filter((job) => {
                    return allJobInfo[job].selected;
                }).map((job) => {
                    return allJobInfo[job].job;
                });
                character.set('characterJobs', selectedJobs);
                character.save();
            }
            return false;
        },
        toggleJob(playerJob) {
            if(playerJob.selected) {
                console.log('Adding: ' + playerJob.job.get('name'));
            } else {
                console.log('Removing: ' + playerJob.job.get('name'));
            }
        }
    }
});
