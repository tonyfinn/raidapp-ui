import Ember from 'ember';

export default Ember.Component.extend({
    sessionService: Ember.inject.service('user-session'),
    alerts: Ember.inject.service('alerts'),
    actions: {
        login() {
            var alerts = this.get('alerts');
            this.get('sessionService').login(this.get('session')).then(() => {
                alerts.clear();
                alerts.addConfirmation('You have logged in');
            }).catch(err => {
                alerts.clear();
                alerts.addError(err.errors[0].detail);
            });
            return false;
        }
    }
});
