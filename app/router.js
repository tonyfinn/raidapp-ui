import Ember from 'ember';
import config from './config/environment';

const Router = Ember.Router.extend({
  location: config.locationType,
  rootURL: config.rootURL
});

Router.map(function() {
  this.route('raids', function() {
      this.route('view', { path: '/:raid_id' });
  });
  this.route('jobs', function () {
  });

  this.route('users', function () {
      this.route('register');
      this.route('login');
  });
  this.route('characters', {}, function() {
    this.route('new', {});
    this.route('edit', { path: '/:character_id' });
  });
  this.route('raid-teams', {}, function() {
    this.route('new', {});
    this.route('view', { path: '/:team_id' });
  });
});

export default Router;
