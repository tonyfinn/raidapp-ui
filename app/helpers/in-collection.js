import Ember from 'ember';

export function inCollection(params/*, hash*/) {
    var collection = params[0];
    var target = params[1];
    return collection.filter(item => {
        return item === target;
    }).length > 0;
}

export default Ember.Helper.helper(inCollection);
