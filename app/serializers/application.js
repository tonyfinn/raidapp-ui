import Ember from 'ember';
import DS from 'ember-data';

export default DS.JSONAPISerializer.extend({
    keyForAttribute(attr) {
        return Ember.String.underscore(attr);
    },
    modelNameFromPayloadKey(key) {
        return Ember.String.dasherize(this._super(key));
    },
    payloadKeyFromModelName(name) {
        return Ember.String.underscore(this._super(name));
    }
});
