import DS from 'ember-data';

export default DS.Model.extend({
    name: DS.attr('string'),
    shortName: DS.attr('string'),
    size: DS.attr('number', {defaultValue: 8}),
    events: DS.hasMany('events')
});
