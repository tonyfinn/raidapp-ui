import DS from 'ember-data';

export default DS.Model.extend({
    name: DS.attr('string'),
    team: DS.belongsTo('raidTeam'),
    server: DS.belongsTo('server'),
    user: DS.belongsTo('user'),
    jobs: DS.hasMany('job'),
    characterJobs: DS.hasMany('characterJob', { async: true })
});

