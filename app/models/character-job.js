import DS from 'ember-data';

export default DS.Model.extend({
    level: DS.attr('number', { defaultValue: 1 }),
    character: DS.belongsTo('character'),
    job: DS.belongsTo('job')
});

