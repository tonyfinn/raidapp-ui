import Ember from 'ember';

export default Ember.Controller.extend({
    sessionService: Ember.inject.service('user-session'),
    currentUser: Ember.computed.alias('sessionService.currentUser'),
    actions: {
        logout() {
            this.get('sessionService').logout();
        }
    }
});
