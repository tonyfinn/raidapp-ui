import Ember from 'ember';

export default Ember.Route.extend({
    model(params) {
        let raid = this.store.peekRecord('raid', params.raid_id);
        let event = this.store.createRecord('event', {
            name: 'Test Event',
            raid
        });
        event.save();
        return raid;
    }
});
