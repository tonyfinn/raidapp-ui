import Ember from 'ember';

export default Ember.Route.extend({
    model() {
        return this.store.createRecord('session');
    },
    actions: {
        loginCompleted() {
            this.redirectTo('characters');
        }
    }
});
