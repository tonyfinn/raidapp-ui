/* eslint-env node */

module.exports = function(environment) {
  var ENV = {
    modulePrefix: 'raidapp-ui',
    environment: environment,
    rootURL: '/',
    locationType: 'auto',
    EmberENV: {
      FEATURES: {
        // Here you can enable experimental features on an ember canary build
        // e.g. 'with-controller': true
      },
      EXTEND_PROTOTYPES: {
        // Prevent Ember Data from overriding Date.parse.
        Date: false
      }
    },

    APP: {
      // Here you can pass flags/options to your application instance
      // when it is created
    }
  };

  if (environment === 'development') {
    // ENV.APP.LOG_RESOLVER = true;
    // ENV.APP.LOG_ACTIVE_GENERATION = true;
    // ENV.APP.LOG_TRANSITIONS = true;
    // ENV.APP.LOG_TRANSITIONS_INTERNAL = true;
    // ENV.APP.LOG_VIEW_LOOKUPS = true;

    var devHost = 'dev.raiderhub.com';
    var liveReloadHost = `${devHost}:49152`;
    ENV.contentSecurityPolicy = {
       'default-src': `${devHost}`,
       'script-src': `'self' ${devHost} ${liveReloadHost} ws://${liveReloadHost} 'unsafe-eval'`,
       'font-src': `'self' ${devHost}`,
       'connect-src': `'self' ${devHost} ws://${liveReloadHost}`,
       'img-src': `'self' ${devHost}`,
       'style-src': `'self' ${devHost} 'unsafe-eval' 'unsafe-inline'`,
       'media-src': `'self' ${devHost}`
    }
  }

  if (environment === 'test') {
    // Testem prefers this...
    ENV.locationType = 'none';

    // keep test console output quieter
    ENV.APP.LOG_ACTIVE_GENERATION = false;
    ENV.APP.LOG_VIEW_LOOKUPS = false;

    ENV.APP.rootElement = '#ember-testing';
  }

  if (environment === 'production') {

  }

  return ENV;
};
