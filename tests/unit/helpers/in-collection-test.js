import { inCollection } from '../../../helpers/in-collection';
import { module, test } from 'qunit';

module('Unit | Helper | in collection');

test('it works', function(assert) {
    assert.ok(inCollection([[2, 42, 5, 3], 42]), 'Included in list');
    assert.notOk(inCollection([[2, 42, 5, 3], 41]), 'Not included in list');

});
