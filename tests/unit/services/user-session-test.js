import Ember from 'ember';
import { moduleFor } from 'ember-qunit';
import test from 'raidapp-ui/tests/ember-sinon-qunit/test';
import stub from 'raidapp-ui/tests/helpers/stub';

var store;

moduleFor('service:user-session', 'Unit | Service | user session', {
});

// Replace this with your real tests.
test('it handles the user not being logged in', function(assert) {
    store = stub.createObj(this, ['queryRecord']);
    store.queryRecord.returns(stub.reject());
    let storeService = Ember.Service.extend(store);
    this.register('service:store', storeService);
    var service = this.subject();
    assert.ok(service);
});
