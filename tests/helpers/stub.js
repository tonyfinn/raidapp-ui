import Ember from 'ember';

let stubHelper = {
    createObj(context, methods=[]) {
        let obj = {};
        for(let method of methods) {
            obj[method] = context.stub();
        }
        return obj;
    },
    resolve(value) {
        return Ember.RSVP.resolve(value);
    },
    reject(value) {
        return Ember.RSVP.reject(value);
    }
};
export default stubHelper;

